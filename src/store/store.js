import Vue from 'vue';
import Vuex from 'vuex';


Vue.use(Vuex)

export const store = new Vuex.Store({
		state: {
			currentStep: 0,
    		questionsList : [
    			"Le risque d'accident est plus élevé lorsque :",
    			"Cela fait déjà quelques heures que je roule en voiture, je commence à exprimer des signes de fatigues mais il ne reste plus que 2 heures avant la fin du trajet. Que devrais-je faire ?",
    			"Est-il plus risqué de conduire le jour ou la nuit ?",
    			"J'ai consommé de l'alcool, je peux en diminuer les effets :",
    			"Je suis sur la route et je m’arrête à un feu rouge. Mon téléphone sonne mais le feu va bientôt passer au vert. C'est peut être un appel important. Que devrais-je faire ?",
                "Je suis conducteur novice, combien de verres d'alcool puis-je boire sans conduire en illegalité?",
    		    "J'ai l'habitude de boire/fumer et ça ne me fait plus d'effet:",
                "J'ai bu (ou pris des drogues), mais j'habite à côté, je prend le volant:",
                "En cas de conduite sous la pluie : \n Pour éviter l'aqualaning je ne dois pas :",
                "En cas de conduite sous la neige: \n Je ne dois pas:",
                "En cas de conduite dans le brouillard, je dois:",
                "Quels sont les équipements obligatoires pour un cycliste selon le code de la route?"


            ],
    		answersList : [
    			{
	    			A: "On ne connaît pas le chemin emprunté",
	    			B: "On connait très bien le chemin qu'on emprunte"
    			},
    			{
    				A: "Mettre la musique plus fort",
    				B: "Trouver un endroit où m’arrêter et boire mon café ",
    				C: "Fumer pourra me réveiller un peu",
    				D: "Accélérer, on dormira une fois arrivé."
    			},
    			{
    				A: "Le jour",
    				B: "La nuit"
    			},
    			{
    				A: "En prenant un bon café ou une boisson énergisante",
    				B: "En mâchant un chewing-gum",
    				C: "En faisant du sport",
    				D: "En attendant que les effets se disspent d'eux-même"
    			},
    			{
    				A: "Cet appel est sûrement important, je dois y répondre.",
    				B: "Y répondre , je suis à l’arrêt après tout.",
    				C: "Je l'éteins et je me concentre sur la route.",
    				D: "Y répondre grâce a mon kit d'écoute, c'est comme si je parlais a un passager."
    			},
                {
                    A: "1 verre",
                    B: "2 verres",
                    C: "3 verres",
                    D: "0 verres"
                },
                {
                    A: "Je peux consommer plus.",
                    B: "Je ne dois pas consommer avant de conduire.",
                    C: "Cela ne change rien, je ne dépasse pas la limite d'alcool par litre de sang."
                },
                {
                    A: "Oui",
                    B: "Non"
                },
                {
                    A: "Diminuer la vitesse",
                    B: "Bien gonfler mes pneus",
                    C: "Observer les traces étroites laissées par les autres voitures",
                    D: "Ne pas considérer l'état de mes pneus"
                },
                {
                    A: "Réduire ma vitesse",
                    B: "Prévoir une diminution de la distance de freinage",
                    C: "Allumer les feux de croisement et les feux de brouillard avant et arrière en cas de forte chutes",
                    D: "Laisser la priorité aux engins de salage et circuler dans leurs traces"
                },
                {
                    A: "Augmenter mon allure",
                    B: "Diminuer mon allure",
                    C: "Adapter mon allure à ma visibilité"
                },
                {
                    A: "Deux freins, deux feux de signalisation, des dispositifs réfléchissants, un casque.",
                    B: "Deux feux de signalisation, un avertisseur sonore, un gilet jaune, un casque",
                    C: "Un avertisseur sonore, deux freins, deux feux de signalisation, des dispositifs réfléchissants",
                    D: "Des dispositifs réfléchissants, deux freins, deux feux de signalisation, un gilet jaune"
                }

    		],
    		commentList : [
    			"C'est en conduisant dans les chemins habituels qu'on a tendance à relâcher son attention et donc à augmenter le risque d'accident.",
    			"Faire une sieste est le meilleur moyen de reprendre ses forces. Le café n'a un effet que temporaire qui met du temps à arriver mais une pause café est toujours un bon prétexte.",
    			"Moins de circulation, mais plus de dangers : la nuit représente 10% du trafic mais 43% des tués et un tiers des blessés hospitalisés.",
    			"Le seul moyen qu'a l'organisme de diminuer le taux d'alcool dans le sang est de filtrer l'alcool avec le foie qui filtre à temps constant. \n La seule façon de faire diminuer son taux d'alcool est donc d'attendre.",
    			"Que ce soit avec un kit ou un téléphone simple , votre concentration est réduite de 30 a 70% lorsque vous communiquez via un appareil connecté. Il est également interdit d'utiliser des appareils mobiles au volant, vous risquez donc une très grosse amende.",
                "Pour un conducteur novice, la tolérance zéro est appliquée.",
                "Ne pas remarquer les effets par habitude de consommer est encore plus dangereux que lors de la première consommation.",
                "Le trajet diminue, mais le risque reste le même.",
                "Des pneus usés augmentent grandement le risque d'aquaplanning.",
                "Circuler proche des véhicules de salage présente un grand risque d'accident",
                "Le manque de visibilité diminue le temps de réaction",
                "Le casque homologué n'est obligatoire que pour les enfants de moins de 12 ans mais il reste fortement conseillé pour les plus âgés. Le gilet, lui, n'est obligatoire que de nuit pour les cyclistes (et leurs passagers) pour un style absolu."
            ],
    		correctionList : [
    			"B",
    			"B",
    			"B",
    			"D",
    			"C",
                "D",
                "B",
                "B",
                "D",
                "D",
                "C",
                "C"

    		],
    		resultIsCorrect : true,
    		score: 0
		},
		getters: {
			currentQuestion (state) {
				return state.questionsList[state.currentStep];
			},
			currentAnswerList (state) {
				return state.answersList[state.currentStep];
			},
			currentComment (state) {
				return state.commentList[state.currentStep-1];
			}
		},
		mutations: {
			answerWasCorrect (state) {
				state.resultIsCorrect = true;
				state.score++;
			},
			answerWasWrong (state) {
				state.resultIsCorrect = false;
			},
			goToNextStep(state) {
				state.currentStep++;
			}
		},
		actions: {
			correctingTheAnwser({commit, state}, answerTag) {
				var expectedAnswer = state.correctionList[state.currentStep]
				if (answerTag === expectedAnswer)
				{
					commit('answerWasCorrect');
				}
				else
				{
					commit('answerWasWrong');
				}
				commit('goToNextStep');
			}
		},
		modules: {
			
		},
	})